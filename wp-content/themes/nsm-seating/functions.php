<?php
/** Start the engine */
require_once( get_template_directory() . '/lib/init.php' );

function child_do_doctype() {
?>
<!DOCTYPE html><html lang="en-US"><head>
<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />

   <?php
    $ie = strpos($_SERVER['HTTP_USER_AGENT'],"IE");
    if($ie) { ?>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
    <?php } ?>

<?php
}

function custom_header() {
    ?>

<div class="header row-fluid">
<div class="logo span4">
<a href="<?=get_bloginfo('home'); ?>" alt="<?php print get_bloginfo('name') .' - '. get_bloginfo('description'); ?>" class="svg-ex">
<img class="visible-desktop" src="<?=get_bloginfo('stylesheet_directory'); ?>/images/st.png" width="100%" title="<?php print get_bloginfo('name') .' - '. get_bloginfo('description'); ?>">
<img class="hidden-desktop" src="<?=get_bloginfo('stylesheet_directory'); ?>/images/nsm_logo_mobile.svg" title="<?php print get_bloginfo('name') .' - '. get_bloginfo('description'); ?>">
</a>
</div>
<?php
    if ( is_active_sidebar( 'header-right' ) || has_action( 'genesis_header_right' ) ) {
        echo '<div class="widget-area search-box span8">';
        do_action( 'genesis_header_right' );
        add_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
        dynamic_sidebar( 'header-right' );
        remove_filter( 'wp_nav_menu_args', 'genesis_header_menu_args' );
        echo '</div><!-- end .widget-area -->';
    }?>
</div>
<?php }


/** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', 'NSM-Seating' );
define( 'CHILD_THEME_URL', 'http://www.artversion.com' );
/** Add HTML5 doctype */
remove_action( 'genesis_doctype', 'genesis_do_doctype' );
add_action( 'genesis_doctype', 'child_do_doctype' );

add_action( 'genesis_before_header', create_function( '', 'echo "<header><div id=\"header\">";' ) );
add_action( 'genesis_after_header', create_function( '', 'echo "</div></header>";' ) );
add_action( 'genesis_before_content', create_function( '', 'echo "<article id=\"main-block\">";' ) );
add_action( 'genesis_after_content', create_function( '', 'echo "</article>";' ) );
add_action( 'genesis_before_footer', create_function( '', 'echo "<footer><div id=\"footer-inner-wrap\">";' ) );
add_action( 'genesis_after_footer', create_function( '', 'echo "</div></footer>";' ) );

remove_action('genesis_header', 'genesis_do_header');
remove_action('genesis_header', 'genesis_header_markup_open', 5);
remove_action('genesis_header', 'genesis_header_markup_close', 15);

add_action('genesis_after_header', 'custom_header');

/* jQuery CDN
--------------------------------------------------------------------------------------- */
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://code.jquery.com/jquery-1.11.3.min.js", [], false, true);
   wp_enqueue_script('jquery');
}


/**
 * The plugins load their JS in the header, which should not.
 */
function reQueuePlugins() {
    if (!is_admin()) {
        $jsLibs = [
            'jquery.scrollTo'   => get_option('siteurl').'/wp-content/plugins/spider-faq/elements/jquery.scrollTo.js',
            'loewy_blog'        => get_option('siteurl').'/wp-content/plugins/spider-faq/elements/loewy_blog.js',
            'tntscript1'        => get_option('siteurl').'/wp-content/themes/nsm-seating/js/video-list-manager.min.js',
        ];
        $cssLibs = [
            'twitter-bootstrap',
            'nsm-seating',
            'contact-form-7',
            'fv_flowplayer',
            'default-template',
            'smart_stylesheet',
            'faq-style',
            'tntColorbox1',
            'tntstyle1',
            'tablepress-default',
            'popup-maker-site',
        ];

        foreach ($jsLibs as $name => $file) {
            wp_deregister_script($name);
            wp_register_script($name, $file, ['jquery'], false, true);
            wp_enqueue_script($name);
        }

        foreach ($cssLibs as $name) {
            wp_dequeue_style($name);
            wp_deregister_style($name);
        }

    }
}
add_action('wp_head', 'reQueuePlugins', 1);


/* Twitter Bootstrap CSS
--------------------------------------------------------------------------------------- */
add_action('genesis_meta', 'link_twitter_bootstrap');
function link_twitter_bootstrap() {
    wp_enqueue_style('twitter-bootstrap', 'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css');
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">'."\r";
}
/**
 * Twitter Bootstrap JS
 */
function bootstrap_js_loader() {
    wp_enqueue_script('bootstrap', 'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js', array('jquery'), false, true );
    wp_enqueue_script('modernizer', get_stylesheet_directory_uri().'/js/modernizr.js', array('jquery'),'1.2', true );

}
add_action('wp_enqueue_scripts', 'bootstrap_js_loader');

/**
 * Modify GravityForm submit buttons to add bootstrap style to them
 */
function form_submit_button ( $button, $form ){
    $button  = str_replace('gform_button button', 'gform_button btn btn-primary', $button );
    return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );


/**
 * Function to load theme dependencies.
 *
 * Looks like the core script is the artversion file, as it sets a lot of functionality on page load
 */
function nsmJsLoader() {
    wp_enqueue_script('bxslider',         get_stylesheet_directory_uri().'/js/jquery.bxslider.min.js',      array('jquery'), false, true );
    wp_enqueue_script('jquery-newsticker', get_stylesheet_directory_uri().'/js/tweet/jquery.newsticker.js', array('jquery'), false, true );
    wp_enqueue_script('artversion', get_stylesheet_directory_uri().'/js/artversion.js', array('jquery', 'bxslider', 'jquery-newsticker', 'bootstrap'), false, true );
}
add_action('wp_enqueue_scripts', 'nsmJsLoader');

/* Twitter Bootstrap Nav
--------------------------------------------------------------------------------------- */
remove_action('genesis_after_header', 'genesis_do_nav');
add_action('genesis_before_header', 'do_bootstrap_nav');
function do_bootstrap_nav() {
    ?>
    <div id="nav" class="navbar container">
        <div class="wrap navbar-inner">
            <div class="container">
                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="nav-collapse">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'menu' ) ); ?>
                </div>

            </div>
        </div>
    </div>
    <?php
}
/* Additional Scripts and Styles
--------------------------------------------------------------------------------------- */
add_action( 'wp_head', 'child_scripts' );
function child_scripts() {
    $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");

    if($android && $iphone) {
    ?>
        <script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"/></script>
    <?php } ?>
<!--[if IE 10]>
<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"/></script>
<![endif]-->

<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
   <script>
        jQuery(document).ready(function() {
                jQuery('.svg-ex img').each(function() {
                    var src = jQuery(this).attr("src");
                    jQuery(this).attr("src", src.replace(/\.svg$/i, ".png"));

                });

                jQuery('.svg-jpg img').each(function() {
                    var src = jQuery(this).attr("src");
                    jQuery(this).attr("src", src.replace(/\.svg$/i, ".jpg"));

                });

        });
   </script>
<![endif]-->

<!--[if IE 7]>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_directory'); ?>/css/font-awesome-ie7.min.css" />
<![endif]-->
  <?php

}

/* Add browser body class
--------------------------------------------------------------------------------------- */
function av_browser_body_class($classes) {
                global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
                if($is_lynx) $classes[] = 'lynx';
                elseif($is_gecko) $classes[] = 'gecko';
                elseif($is_opera) $classes[] = 'opera';
                elseif($is_NS4) $classes[] = 'ns4';
                elseif($is_safari) $classes[] = 'safari';
                elseif($is_chrome) $classes[] = 'chrome';
                elseif($is_IE) {
                        $classes[] = 'ie';
                        if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
                        $classes[] = 'ie'.$browser_version[1];
                } else $classes[] = 'unknown';
                if($is_iphone) $classes[] = 'iphone';
                if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
                         $classes[] = 'osx';
                   } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
                         $classes[] = 'linux';
                   } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
                         $classes[] = 'windows';
                   }
                return $classes;
        }
        add_filter('body_class','av_browser_body_class');


add_action('genesis_after_footer', 'child_load_retna');
function child_load_retna() {
        wp_register_script( 'retna', get_bloginfo('stylesheet_directory').'/js/retina.js' );
        wp_enqueue_script ( 'retna' );
}


/* Add support for structural wraps
--------------------------------------------------------------------------------------- */
add_theme_support( 'genesis-structural-wraps', array( 'header', 'nav', 'subnav', 'inner', 'footer-widgets', 'footer' ) );

/* Add Default Menus: registers Primary and Secondary
--------------------------------------------------------------------------------------- */
add_theme_support ( 'genesis-menus' , array ( 'primary' => 'Primary Navigation Menu' , 'secondary' => 'Secondary Navigation Menu' ) );


/* Add logo
--------------------------------------------------------------------------------------- */
/*

add_filter( 'genesis_seo_title', 'child_header_title', 10, 3 );
function child_header_title( $title, $inside, $wrap ) {

    $inside = sprintf( '<a href="http://nsm-seating.artversion.net/" class="svg-ex"><img src="'. get_stylesheet_directory_uri() .'/images/logox2.svg" title="Logo" />', esc_attr( get_bloginfo( 'name' ) ), get_bloginfo( 'name' ) );

    return sprintf( '<%1$s id="title">%2$s</%1$s>', $wrap, $inside );

}

*/
/* Remove Title & Description
--------------------------------------------------------------------------------------- */

//remove_action( 'genesis_site_title', 'genesis_seo_site_title' );
remove_action( 'genesis_site_description', 'genesis_seo_site_description' );


/* Add container class
--------------------------------------------------------------------------------------- */
function child_before_content_sidebar_wrap() {
    echo '<div class="container-responsive">';
}
add_action('genesis_before_content_sidebar_wrap', 'child_before_content_sidebar_wrap');
function child_after_content_sidebar_wrap() {
    echo '</div><!-- end .container -->';

}

add_action('genesis_after_content_sidebar_wrap', 'child_after_content_sidebar_wrap');
remove_action( 'genesis_after_header', 'genesis_do_subnav' );

add_action( 'genesis_header', 'genesis_do_subnav' );


/* Add support for 3-column footer widgets
--------------------------------------------------------------------------------------- */
add_theme_support( 'genesis-footer-widgets', 3 );

/* Add widgets
--------------------------------------------------------------------------------------- */

/** Home */

/** Before Header */
add_action( 'genesis_before_header', 'artversion_before_header_widget', 'artversion' );
function artversion_before_header_widget() {
    if ( is_active_sidebar( 'before-header-widget' ) ) {
    echo '<div class="before-header-widget"><div class="b-h-inner">';
    dynamic_sidebar( 'before-header-widget' );
    echo '</div></div>';
    }
}

/** After Header */
add_action( 'genesis_after_header', 'artversion_after_header_widget', 'artversion' );
function artversion_after_header_widget() {
    if ( is_active_sidebar( 'after-header-widget' ) ) {
    echo '<div class="after-header-widget">';
    dynamic_sidebar( 'after-header-widget' );
    echo '</div>';
    }
}
/** Before content */
add_action( 'genesis_before_content', 'artversion_before_content_widget', 'artversion' );
function artversion_before_content_widget() {
    if ( is_active_sidebar( 'before-content-widget' ) ) {
    echo '<div class="before-content-widget">';
    dynamic_sidebar( 'before-content-widget' );
    echo '</div>';
    }
}

/** After content */
add_action( 'genesis_after_content', 'artversion_after_content_widget', 'artversion' );
function artversion_after_content_widget() {
    if ( is_active_sidebar( 'after-content-widget' ) ) {
    echo '<div class="after-content-widget">';
    dynamic_sidebar( 'after-content-widget' );
    echo '</div><!-- end .after-header-widget -->';
    }
}

/** Before loop */
add_action( 'genesis_before_loop', 'artversion_before_loop_widget', 'artversion' );
function artversion_before_loop_widget() {
    if ( is_active_sidebar( 'before-loop-widget' ) ) {
    echo '<div class="before-loop-widget">';
    dynamic_sidebar( 'before-loop-widget' );
    echo '</div>';
    }
}

/** After loop */
add_action( 'genesis_after_loop', 'artversion_after_loop_widget', 'artversion' );
function artversion_after_loop_widget() {
    if ( is_active_sidebar( 'after-loop-widget' ) ) {
    echo '<div class="after-loop-widget">';
    dynamic_sidebar( 'after-loop-widget' );
    echo '</div>';
    }
}

/** Before Footer */
add_action( 'genesis_before_footer', 'artversion_genesis_before_footer', 'artversion' );
function artversion_genesis_before_footer() {
    if ( is_active_sidebar( 'before-footer' ) ) {
    echo '<div id="before-footer">';
    dynamic_sidebar( 'before-footer' );
    echo '</div>';
    }
}

/** After Footer */
add_action( 'genesis_footer', 'artversion_genesis_footer', 'artversion' );
function artversion_genesis_footer() {


    if ( is_active_sidebar( 'footer-copyright' ) ) {
    echo '<div id="footer-copyright">';
    dynamic_sidebar( 'footer-copyright' );
    echo '</div><!-- end .footer_copyright -->';
    }
}


genesis_register_sidebar( array(
    'name'=>'Home Content Widget',
    'id'			=> 'home-top',
    'name'			=> __( 'Home Content Top Widget', 'artversion' ),
    'description'	=> __( 'This is the home top content widget section.', 'artversion' ),
    'before_widget' => '<div id="home-top-%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h2 class="home-h4">','after_title'=>'</h2>'
) );
genesis_register_sidebar( array(
    'name'=>'Home Left Widget',
    'id'			=> 'home-left',
    'name'			=> __( 'Home Left Widget', 'artversion' ),
    'description'	=> __( 'This is the home left widget section.', 'artversion' ),
    'before_widget' => '<div class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h2 class="home-h4">','after_title'=>'</h2>'
) );
genesis_register_sidebar( array(
    'name'=>'Home Right Widget',
    'id'			=> 'home-right',
    'name'			=> __( 'Home Right Widget', 'artversion' ),
    'description'	=> __( 'This is the home right widget section.', 'artversion' ),
    'before_widget' => '<div class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h2 class="home-h4">','after_title'=>'</h2>'

) );

/* Register widgets
--------------------------------------------------------------------------------------- */
genesis_register_sidebar( array(
    'name'=>'Before Header Widget Area',
    'id'			=> 'before-header-widget',
    'name'			=> __( 'Before Header Widget Area', 'artversion' ),
    'description'	=> __( 'This is before header widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'After Header Widget Area',
    'id'			=> 'after-header-widget',
    'name'			=> __( 'After Header Widget Area', 'artversion' ),
    'description'	=> __( 'This is after header widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'Before Content Widget',
    'id'			=> 'before-content-widget',
    'name'			=> __( 'Before Content Widget', 'artversion' ),
    'description'	=> __( 'This is before content widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'After Content Widget',
    'id'			=> 'after-content-widget',
    'name'			=> __( 'After Content Widget', 'artversion' ),
    'description'	=> __( 'This is after content widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'Before Loop Widget',
    'id'			=> 'before-loop-widget',
    'name'			=> __( 'Before Loop Widget', 'artversion' ),
    'description'	=> __( 'This is before loop widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'After Loop Widget',
    'id'			=> 'after-loop-widget',
    'name'			=> __( 'After Loop Widget', 'artversion' ),
    'description'	=> __( 'This is the after loop widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'Before Footer Widget',
    'id'			=> 'before-footer',
    'name'			=> __( 'Before Footer Widget', 'artversion' ),
    'description'	=> __( 'This is before footer widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );
genesis_register_sidebar( array(
    'name'=>'Footer Copyright Widget',
    'id'			=> 'footer-copyright',
    'name'			=> __( 'Footer Copyright Widget', 'artversion' ),
    'description'	=> __( 'This is the footer copyright widget section.', 'artversion' ),
    'before_widget' => '<div id="%1$s" class="widget-area %2$s">', 'after_widget'  => '</div>',
    'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
) );

/* Add back to top
--------------------------------------------------------------------------------------- */
add_filter( 'genesis_footer_output', 'child_output_filter', 10, 3 );
function child_output_filter( $backtotop_text, $creds_text ) {
    $backtotop_text = '[footer_backtotop text="<img src=\'http://www.nsm-seating.com/wp-content/themes/nsm-seating/images/back-to-top1.png\' />"]';
    $creds_text = '';
    return '<div class="gototop">' . $backtotop_text . '</div>';
}

/* Branding
--------------------------------------------------------------------------------------- */
function art_footer_admin () {
echo '<small> <a href="http://www.artversion.com" target="_blank" title="Web Design">Web Design</a> by <a href="http://www.artversion.com" target="_blank" title="Web Design Company">ArtVersion</a></small>';
}
add_filter('admin_footer_text', 'art_footer_admin');

/* Security
--------------------------------------------------------------------------------------- */
remove_action( 'wp_head', 'wp_generator' );

/* Add Shortcodec and PHP in the widgets
--------------------------------------------------------------------------------------- */

add_filter('widget_text', 'do_shortcode');
add_filter('widget_text', 'php_text', 99);
function php_text($text) {
 if (strpos($text, '<' . '?') !== false) {
 ob_start();
 eval('?' . '>' . $text);
 $text = ob_get_contents();
 ob_end_clean();
 }
 return $text;
}
add_filter('the_content', 'php_content', 99);

function php_content($text) {
 if (strpos($text, '<' . '?') !== false) {
 ob_start();
 eval('?' . '>' . $text);
 $text = ob_get_contents();
 ob_end_clean();
 }
 return $text;
}

/* Add Slider Image Size and custom thumb sizes

--------------------------------------------------------------------------------------- */
function set_flexslider_hg_rotators()
{
  $rotators = array();
  $rotators['homepage'] = array( 'size' => 'homepage-rotator' );
  return $rotators;
}
add_filter('flexslider_hg_rotators', 'set_flexslider_hg_rotators');
add_image_size( 'homepage-rotator', '1920', '9999', true );

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(150, 150, TRUE);
add_image_size( 'press-thumb', 200, 300, TRUE );
add_image_size( 'long-thumb', 300, 600, TRUE );
add_image_size( 'medium-thumb', 300, 300, TRUE );
add_image_size( 'big-thumb', 600, 600, TRUE );
add_image_size( 'slide', 1400, 9999, TRUE );

/* Add custom search
--------------------------------------------------------------------------------------- */
add_filter('genesis_search_button_text', 'art_custom_search_button_text');
function art_custom_search_button_text($text) {
    return esc_attr('Go');
}
function art_custom_search_text( $text ) {
return esc_attr( 'Search Site...' );
}
add_filter('genesis_search_text', 'art_custom_search_text');



/** Content clear-fix */
add_action( 'genesis_after_content_sidebar_wrap', 'clear_fix' );
function clear_fix() {
    ?>
    <div class="clear"></div>
    <?php
}

/* Fix
--------------------------------------------------------------------------------------- */
/** empty paragraph fix */
add_filter('the_content', 'shortcode_empty_paragraph_fix');
    function shortcode_empty_paragraph_fix($content)
    {
        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );
        $content = strtr($content, $array);
        return $content;
    }

/** fix readmore jump */
function no_more_jumping($post) {
    return '<a class="read-more" href="'.get_permalink($post->ID).'">'.'Continue Reading'.'</a>';
}
add_filter('excerpt_more', 'no_more_jumping');

/* no more self ping/trackback */
function disable_self_ping( &$links ) {
    foreach ( $links as $l => $link )
        if ( 0 === strpos( $link, get_option( 'home' ) ) )
            unset($links[$l]);
}
add_action( 'pre_ping', 'disable_self_ping' );

/** add post thumbnail ( featured image ) to rss feed */
function art_rss_post_thumbnail($content) {
    global $post;
    if(has_post_thumbnail($post->ID)) {
        $content = '<p>' . get_the_post_thumbnail($post->ID) .
        '</p>' . get_the_content();
    }
    return $content;
}
add_filter('the_excerpt_rss', 'art_rss_post_thumbnail');

add_filter('the_content_feed', 'art_rss_post_thumbnail');
/* Remove deprecated menu pages widget
--------------------------------------------------------------------------------------- */
function m_widgets(){
    unregister_widget( 'Genesis_eNews_Updates' );
    unregister_widget( 'Genesis_Featured_Page' );
    unregister_widget( 'Genesis_Latest_Tweets_Widget' );
    unregister_widget( 'Genesis_Widget_Menu_Categories' );
    unregister_widget( 'Genesis_Menu_Pages_Widget' );
    unregister_widget( 'Genesis_User_Profile_Widget' );
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Links' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
    unregister_widget( 'Akismet_Widget' );
    }
add_action( "widgets_init", "m_widgets" );

/* Add Shortcode
--------------------------------------------------------------------------------------- */

/** protect emails */

function art_hide_email( $atts, $content ){
    return '<a href="'.antispambot("mailto:".$content).'">'.antispambot($content).'</a>';
}
add_shortcode( 'email', 'art_hide_email' );


/**
 *  bootstrap grids and layouts
 * all clasess can be doubled, [box class="class1 class2"] [/box]
 */

/**
 * row [row][/row] with fluid type [row type=-fluid][/row]
 * row places one item on top of eachother if they are bigger than 50% o0f the content
 * ----
 * -----
 * row-fluid packs them inline
 * ---- --
 */

function art_row($params, $content = null) {
    extract(shortcode_atts(array(
        'type' => ''
    ), $params));
    return
    '<div class="row'.$type.'">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('row','art_row');

/* row span class 1 - 12
class 12x1  = 12 boxes class 4 x 3 = 3 boxes
class 4 + class 8 = 2 boxes 1x4 + 1x8
class 6 + class 6 = 2 boxes 6x1 + 6x1
class 12 = 1 box 1x12 offset is distance from left side in frid lines 1-12
 */

function art_row_span($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => '1',
        'offset' => ''
    ), $params));
    return
    '<div class="span'.$class.' offset'.$offset.'">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('rowspan','art_row_span');

/* row span level 2 ( inner row )class 1 - 12 */

function art_row_spanl2($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => '1',
        'offset' => ''
    ), $params));
    return
    '<div class="span'.$class.' offset'.$offset.'">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('rowspanl2','art_row_spanl2');

/* thumbnails */

function art_thumbnails($params, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<ul class="thumbnails">' .
    do_shortcode($content) .
    '</ul>';
}
add_shortcode('thumbnails','art_thumbnails');


function art_thumb($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => '1',
        'offset' => ''
    ), $params));
    return
    '<li class="span'.$class.' offset'.$offset.'"><div class="thumbnail">' .
    do_shortcode($content) .
    '</div></li>';
}
add_shortcode('thumb','art_thumb');



/* responsive hide show classes
phone = 767px and below
tablet = 979px to 768px
desktop 980 and up
[box class=visible-phone][/box]
[box class=visible-tablet][/box]
[box class=visible-desktop][/box]
[box class=hidden-phone][/box]
[box class=hidden-tablet][/box]
[box class=hidden-desktop][/box]
*/

function art_responsive_show_hide($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => 'show'
    ), $params));
    return
    '<div class="box '.$class.'">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('box','art_responsive_show_hide');


/* white icons [icon type=search size=normal] */
 function art_icon_white( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "class" => 'ico',
      "type" => 'type',
      "size" => 'normal'
    ), $atts));    return '<div class="'.$class.'"><i class="icon icon-' . $type . ' icon-white icon-' . $size .'"></i></div>';
  }
add_shortcode('whiteico','art_icon_white');

/* font-awersome implementation [icon type=search size=normal] */
function art_home_icon( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "class" => 'class',
      "type" => 'type',
      "size" => 'normal'
    ), $atts));
    return '<div class="'.$class.'"><i class="icon icon-' . $type . ' icon-' . $size .'"></i></div>';
}

add_shortcode('homeicon','art_home_icon');


/* font-awersome implementation [icon type=search size=normal] */
function art_icon( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "class" => 'class',
      "type" => 'type',
      "size" => 'normal'
    ), $atts));
    return '<i class="icon icon-' . $type . ' icon-' . $size .' icon-' . $class . '"></i>';
}
add_shortcode('icon','art_icon');

/* well blocqoote like box around element [well size=large]  [well size=small] */
function art_well( $atts, $content = null ) {
   extract(shortcode_atts(array(
    "size" => 'size'
    ), $atts));
   return '<div class="well well-' . $size . '">' . do_shortcode( $content ) . '</div>';
}
add_shortcode('well','art_well');


/* social custo add this */
/* add to 16 shortcode [addonesix] */
function art_addto16($atts, $content = null) {
    return '
<div class="soc-box small"><div class="addthis_toolbox addthis_default_style addthis_16x16_style">
<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_pinterest_share"></a>
<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=undefined"></script>
</div>
';
}
add_shortcode("addonesix", "art_addto16");


/* add to 32 shortcode [addonesix] */
function art_addto32($atts, $content = null) {
    return '
<div class="soc-box big"><div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_pinterest_share"></a>
<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=undefined"></script>
</div>
';
}
add_shortcode("addthreetwo", "art_addto32");
/*  */
function art_roll($atts, $content = null) {
    return '<div class="img-roll">'.$content.'</div>';
}
add_shortcode('img-roll', 'art_roll');


/* side [colone][/colone] */
function art_colone($atts, $content = null) {
    return '<div class="colone">'.$content.'</div>';
}
add_shortcode('colone', 'art_colone');
/* side [coltwo][/coltwo] */
function art_coltwo($atts, $content = null) {
    return '<div class="coltwo">'.$content.'</div>';
}
add_shortcode('coltwo', 'art_coltwo');


/* side [colthree][/colthree] */
function art_colthree($atts, $content = null) {
    return '<div class="colthree">'.$content.'</div>';
}
add_shortcode('colthree', 'art_colthree');

/* bx slider  [slide] [/slide]*/

function art_Sl($params, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<div class="slider"><ul id="slidertwo">' .
    do_shortcode($content) .
    '</ul></div>';
}
add_shortcode('slide','art_Sl');

function art_Sl_side($params, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<div class="slider mugshots"><ul class="sliderthree">' .
    do_shortcode($content) .
    '</ul></div>';
}
add_shortcode('slides','art_Sl_side');


function art_Sl_side_two($params, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<div class="slider small-mgs"><ul id="sliderfour">' .
    do_shortcode($content) .
    '</ul></div>';
}
add_shortcode('slidetwo','art_Sl_side_two');

function art_Sl_side_tree($params, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<div class="slider mugshots"><ul id="sliderfive">' .
    do_shortcode($content) .
    '</ul></div>';
}
add_shortcode('slidetree','art_Sl_side_three');


function art_Sl_Item($atts, $content = null) {
    return '<li>'.$content.'</li>';
}
add_shortcode("slideitem", "art_Sl_Item");


/* modal box button [mbutton href=#modalone][/mbutton] */

function art_modal_button($atts, $content = null) {
    extract(shortcode_atts(array(
        "href" => '#',
    ), $atts));
    return '<a href="'.$href.'" role="button" class="btn" data-toggle="modal">'.$content.'</a>';
}
add_shortcode("mbutton", "art_modal_button");

/* modal box [mbox id=#modalone][/mbox] */

function art_modal_box($params, $content = null) {
    extract(shortcode_atts(array(
        "id" => '#',
    ), $params));
    return
    '<div id="'.$id.'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('mbox','art_modal_box');

/* modal header [mheader][/mheader] */
function art_modal_header($atts, $content = null) {
    return '<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-off"></i></button>
'.$content.'
</div>';
}
add_shortcode("mheader", "art_modal_header");


/* modal body [mbody][/mbody] */
function art_modal_body($atts, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<div class="modal-body">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode("mbody", "art_modal_body");

/* modal footer [mfooter][/mfooter] */
function art_modal_footer($atts, $content = null) {
    return '<div class="modal-footer">
'.$content.'
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>';
}
add_shortcode("mfooter", "art_modal_footer");


/* clear shortcode [clear] */
function art_clear($atts, $content = null) {
    return '<div class="clear"></div>';
}
add_shortcode("clear", "art_clear");

/* clear shortcode [clearone] */
function art_clear_one($atts, $content = null) {
    return '<div class="clear typeone"></div>';
}
add_shortcode("clearone", "art_clear_one");

/* clear shortcode [border] */

function art_border($atts, $content = null) {

    return '<div class="border"></div>';

}

add_shortcode("border", "art_border");


/** cilapse */

function art_step($atts, $content = null) {
    return '<div class="step-text">'.$content.'</div>';
}
add_shortcode("step", "art_step");



function art_stepbutton( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'target' => 'caption',
      ), $atts ) );

   return '<button type="button" class="btn" data-toggle="collapse" data-target="' . esc_attr($target) . '">' . $content . '</button>';
}
add_shortcode("stepbutton", "art_stepbutton");


function art_colapsediv( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'id' => 'caption',
      'class' => 'caption',
      ), $atts ) );

   return '<div id="' . esc_attr($id) . '" class="' . esc_attr($class) . ' collapse">' . $content . '</div>';
}
add_shortcode("colapsediv", "art_colapsediv");

function art_question_button( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'target' => 'caption',
      ), $atts ) );

   return '<button type="button" class="btn" data-toggle="collapse" data-target="' . esc_attr($target) . '">' . $content . '</button>';
}
add_shortcode("question", "art_question_button");


function art_answer_div( $atts, $content = null ) {
   extract( shortcode_atts( array(
      'id' => 'caption',
      'class' => 'caption',
      ), $atts ) );

   return '<div id="' . esc_attr($id) . '" class="' . esc_attr($class) . ' collapse">' . $content . '</div>';
}
add_shortcode("answer", "art_answer_div");

/* modal body [white][/white] */
function art_white_body($atts, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<ul class="text-ul"><li>' .
    do_shortcode($content) .
    '</li></ul>';
}
add_shortcode("white", "art_white_body");


function art_white_nb($atts, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<ul class="text-ul nb"><li>' .
    do_shortcode($content) .
    '</li></ul>';
}
add_shortcode("whitenb", "art_white_nb");


function art_jobs_top($atts, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<div class="row-fluid">
    <div class="span12" id="top-text">' .
    do_shortcode($content) .
    '</div>
    <div class="clear">
    </div>';
}
add_shortcode("jobtop", "art_jobs_top");

/* year shortcode [year] */

function year_shortcode() {

 $year = date('Y');

 return $year;

}

add_shortcode('year', 'year_shortcode');


function art_faslide() {

if( function_exists('FA_display_slider') ){
    $faslide = FA_display_slider(694);
}

 return $faslide;

}

add_shortcode('faslide', 'art_faslide');



function art_hslide() {

if(function_exists('show_flexslider_rotator')){
    $hslide = show_flexslider_rotator( 'homepage' );
}

 return $hslide;

}

add_shortcode('homeslide', 'art_hslide');



function art_post_sidebar($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => 'side'
    ), $params));
    return
    '<div class="post-box '.$class.'">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('recentbox','art_post_sidebar');


function art_text_widget($params, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'title'
    ), $params));
    return
    '
<div class="widget widget_text custom_left">
    <div class="widget-wrap">
    <h4 class="widgettitle"> '.$title.'</h4>
        <div class="textwidget">

        <div class="text-box"><ul><li>' .
        do_shortcode($content) .
        '</li></ul></div>

        </div>
    </div>
</div>';
}
add_shortcode('textwidget','art_text_widget');




/** link widget left side and body */

function art_link_widget($params, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'title'
    ), $params));
    return
    '
<div class="widget widget_text custom_body">
    <div class="widget-wrap">
    <h4 class="widgettitle"> '.$title.'</h4>
        <div class="textwidget">

        <div class="link-box"><ul>' .
        do_shortcode($content) .
        '</ul></div>

        </div>
    </div>
</div>';
}
add_shortcode('linkwidget','art_link_widget');


/** link box right [linkwidgetr][/linkwidgetr] */

function art_linkwidgetr($atts, $content = null) {
    extract(shortcode_atts(array(
    ), $params));
    return
    '<ul class="text-ul-link">' .
    do_shortcode($content) .
    '</ul>';
}
add_shortcode("linkwidgetr", "art_linkwidgetr");


function art_text_widget_link($params, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'title'
    ), $params));
    return
    ' <li>' . $content .	'</li>';
}
add_shortcode('linkli','art_text_widget_link');



function art_recent_one() {
 return '

<div class="widget widget_recent_entries" id="recent-posts-plus-3">
<div class="widget-wrap">
<h4 class="widgettitle">Recent Blog Posts</h4>
<ul>
<?php
global $post;
$myposts = get_posts(\'numberposts=5&category=1\');
foreach($myposts as $post) :
?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach;?>
<?php wp_reset_query(); ?>
</ul>
</div>
</div>
';
}

add_shortcode('recentblog', 'art_recent_one');


function art_recent_jobs() {
 return '
<ul>
<?php
global $post;
$myposts = get_posts(\'numberposts=5&category=9\');
foreach($myposts as $post) :
?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach;?>
<?php wp_reset_query(); ?>
</ul>
';
}

add_shortcode('recentjobs', 'art_recent_jobs');


function art_recent_allr() {
 return '
<div class="widget widget_recent_entries" id="recent-posts-plus-3">
<div class="widget-wrap">
<h4 class="widgettitle">Recent Posts</h4>
<ul>
<?php
global $post;
$myposts = get_posts(\'numberposts=5&category=-1\');
foreach($myposts as $post) :
?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach;?>
<?php wp_reset_query(); ?>
</ul>
</div>
</div>
';
}

add_shortcode('allrecent', 'art_recent_allr');




function art_rec_jobs() {
 return '
<div class="widget widget_recent_entries" id="recent-posts-plus-3">
<div class="widget-wrap">
<h4 class="widgettitle">Recent NSM Openings</h4>
<ul>
<?php
global $post;
$myposts = get_posts(\'numberposts=5&category=9\');
foreach($myposts as $post) :
?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach;?>
<?php wp_reset_query(); ?>
</ul>
</div>
</div>
';
}

add_shortcode('alljobs', 'art_rec_jobs');






/** image sizes */

/** image size [img1][/img1] */

function art_imgone($params, $content = null) {
    extract(shortcode_atts(array(
        'class' => 'side'
    ), $params));
    return
    '<div class="img-'.$class.'">' .
    do_shortcode($content) .
    '</div>';
}
add_shortcode('imgone','art_imgone');






/* Home Shortcode

--------------------------------------------------------------------------------------- */
/* [tweet][/tweet] */
function art_tweet($atts, $content = null) {

    return '<div class="twit-box row-fluid"><div id="twitt-left" class="span4"><h2>Follow us<br /> on Twitter</h2><i class="icon-twitter"></i></div><div id="ticker" class="span8"><div id="twitter-feed">'.$content.'</div></div></div>';

}

add_shortcode('tweet', 'art_tweet');





/* [homebox type=leftbox][/homebox] */
function art_homeboxl($atts, $content = null) {

    extract(shortcode_atts(array(

    ), $params));

    return '<div class="homebox homeleft"> '.do_shortcode($content).' </div>';

}

add_shortcode('homeboxl', 'art_homeboxl');
function art_homeboxr($atts, $content = null) {

    extract(shortcode_atts(array(

    ), $params));

    return '<div class="homebox home-right"> '.do_shortcode($content).' </div>';

}

add_shortcode('homeboxr', 'art_homeboxr');
/* [recent-posts] */


function my_recent_posts_shortcode($atts){

 $q = new WP_Query(

   array( 'orderby' => 'date', 'posts_per_page' => '1')

 );
$list = '<ul class="recent-posts">';
while($q->have_posts()) : $q->the_post();
 $list .= '<li>' . get_the_date() . ' - <a href="' . get_permalink() . '">' . get_the_title() . '</a>' . '<br /><p>' . get_the_excerpt() . '</p></li>';
endwhile;
wp_reset_query();
return $list . '</ul>';
}
add_shortcode('recent-posts', 'my_recent_posts_shortcode');


/** Custom Read More link for Genesis Post Excerpts */

function custom_read_more_link() {

return '... <br /> <br /><a class="readmore" href="' .get_permalink() .'" rel="nofollow"><span>Read More</span></a>';

}

add_filter( 'excerpt_more', 'custom_read_more_link' );

add_filter( 'get_the_content_more_link', 'custom_read_more_link' );

add_filter( 'the_content_more_link', 'custom_read_more_link' );


/* [imgbox][/imgbox] */
function art_imgbox($params, $content = null) {

    extract(shortcode_atts(array(

    ), $params));

    return

    '<div class="imgbox">' .

    do_shortcode($content) .

    '<div class="clear typeone"></div></div>';

}

add_shortcode('imgbox','art_imgbox');


/* [textbox][/textbox] */
function art_textbox($params, $content = null) {

    extract(shortcode_atts(array(

    ), $params));

    return

    '<div class="textbox">' .

    do_shortcode($content) .

    '</div>';

}

add_shortcode('textbox','art_textbox');

/* [arrowtbox][/arrowbox] */
function art_arrowbox($params, $content = null) {

    extract(shortcode_atts(array(

    ), $params));

    return

    '<div id="arrow-box-container"><div class="arrowtbox">' .

    do_shortcode($content) .

    '<div id="arrow-down"></div></div></div>';

}

add_shortcode('arrowbox','art_arrowbox');



/* popover [popover] */

function art_pop( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "title" => 'class',
      "popcontent" => 'type'
    ), $atts));
    return '<a class="pop" data-original-title="'.$title.'" data-content="'.$popcontent.'" rel="popover" href="#" data-placement="left" data-toggle="popover">' . $content . '</a>';
}

add_shortcode('popover','art_pop');



/* popover [tooltip] */

function art_tooltip( $atts, $content = null ) {
    extract(shortcode_atts(array(
      "title" => 'title'
    ), $atts));
    return '<a title="'.$title.'" class="tp" rel="tooltip" href="#">' . $content . '</a>';
}

add_shortcode('tooltip','art_tooltip');


/* [allbox][/allbox] */
function art_allbox($params, $content = null) {

    extract(shortcode_atts(array(

    ), $params));

    return

    '<div class="allbox">' .

    do_shortcode($content) .

    '</div>';

}

add_shortcode('allbox','art_allbox');



function art_bannerone($atts, $content = null) {
    extract(shortcode_atts(array(
      "title" => 'title'
    ), $atts));
    return '
    <div class="random-image">
    <img src="<?php echo get_site_url(); ?>/wp-content/uploads/banners/one/<?php echo rand(1,10);?>.jpg" alt="Random Image" />
    <div class="caption-box"><span>'. $title . '</span></div>
    </div>
    ';

}
add_shortcode("bannerone", "art_bannerone");


function art_bannertwo($atts, $content = null) {
    extract(shortcode_atts(array(
      "title" => 'title'
    ), $atts));
    return '
    <div class="random-image">
    <img src="<?php echo get_site_url(); ?>/wp-content/uploads/banners/one/image.php" alt="Random Image" />
    <div class="caption-box"><span>' . $title . '</span></div>
    </div>
    ';

}
add_shortcode("bannertwo", "art_bannertwo");




/* featured posts with thumbnails per category */


add_shortcode( 'display-posts', 'be_display_posts_shortcode' );

function be_display_posts_shortcode( $atts ) {



    // Original Attributes, for filters

    $original_atts = $atts;



    // Pull in shortcode attributes and set defaults

    $atts = shortcode_atts( array(

        'author'              => '',

        'category'            => '',

        'date_format'         => '(n/j/Y)',

        'id'                  => false,

        'ignore_sticky_posts' => false,

        'image_size'          => false,

        'include_content'     => false,

        'include_date'        => false,

        'include_excerpt'     => false,

        'meta_key'            => '',

        'no_posts_message'    => '',

        'offset'              => 0,

        'order'               => 'DESC',

        'orderby'             => 'date',

        'post_parent'         => false,

        'post_status'         => 'publish',

        'post_type'           => 'post',

        'posts_per_page'      => '10',

        'tag'                 => '',

        'tax_operator'        => 'IN',

        'tax_term'            => false,

        'taxonomy'            => false,

        'wrapper'             => 'ul',

    ), $atts );



    $author = sanitize_text_field( $atts['author'] );

    $category = sanitize_text_field( $atts['category'] );

    $date_format = sanitize_text_field( $atts['date_format'] );

    $id = $atts['id']; // Sanitized later as an array of integers

    $ignore_sticky_posts = (bool) $atts['ignore_sticky_posts'];

    $image_size = sanitize_key( $atts['image_size'] );

    $include_content = (bool)$atts['include_content'];

    $include_date = (bool)$atts['include_date'];

    $include_excerpt = (bool)$atts['include_excerpt'];

    $meta_key = sanitize_text_field( $atts['meta_key'] );

    $no_posts_message = sanitize_text_field( $atts['no_posts_message'] );

    $offset = intval( $atts['offset'] );

    $order = sanitize_key( $atts['order'] );

    $orderby = sanitize_key( $atts['orderby'] );

    $post_parent = $atts['post_parent']; // Validated later, after check for 'current'

    $post_status = $atts['post_status']; // Validated later as one of a few values

    $post_type = sanitize_text_field( $atts['post_type'] );

    $posts_per_page = intval( $atts['posts_per_page'] );

    $tag = sanitize_text_field( $atts['tag'] );

    $tax_operator = $atts['tax_operator']; // Validated later as one of a few values

    $tax_term = sanitize_text_field( $atts['tax_term'] );

    $taxonomy = sanitize_key( $atts['taxonomy'] );

    $wrapper = sanitize_text_field( $atts['wrapper'] );





    // Set up initial query for post

    $args = array(

        'category_name'       => $category,

        'order'               => $order,

        'orderby'             => $orderby,

        'post_type'           => explode( ',', $post_type ),

        'posts_per_page'      => $posts_per_page,

        'tag'                 => $tag,

    );



    // Ignore Sticky Posts

    if( $ignore_sticky_posts )

        $args['ignore_sticky_posts'] = true;



    // Meta key (for ordering)

    if( !empty( $meta_key ) )

        $args['meta_key'] = $meta_key;



    // If Post IDs

    if( $id ) {

        $posts_in = array_map( 'intval', explode( ',', $id ) );

        $args['post__in'] = $posts_in;

    }



    // Post Author

    if( !empty( $author ) )

        $args['author_name'] = $author;



    // Offset

    if( !empty( $offset ) )

        $args['offset'] = $offset;



    // Post Status

    $post_status = explode( ', ', $post_status );

    $validated = array();

    $available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );

    foreach ( $post_status as $unvalidated )

        if ( in_array( $unvalidated, $available ) )

            $validated[] = $unvalidated;

    if( !empty( $validated ) )

        $args['post_status'] = $validated;





    // If taxonomy attributes, create a taxonomy query

    if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {



        // Term string to array

        $tax_term = explode( ', ', $tax_term );



        // Validate operator

        if( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) )

            $tax_operator = 'IN';



        $tax_args = array(

            'tax_query' => array(

                array(

                    'taxonomy' => $taxonomy,

                    'field'    => 'slug',

                    'terms'    => $tax_term,

                    'operator' => $tax_operator

                )

            )

        );



        // Check for multiple taxonomy queries

        $count = 2;

        $more_tax_queries = false;

        while(

            isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) &&

            isset( $original_atts['tax_' . $count . '_term'] ) && !empty( $original_atts['tax_' . $count . '_term'] )

        ):



            // Sanitize values

            $more_tax_queries = true;

            $taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );

             $terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );

             $tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts['tax_' . $count . '_operator'] : 'IN';

             $tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';



             $tax_args['tax_query'][] = array(

                 'taxonomy' => $taxonomy,

                 'field' => 'slug',

                 'terms' => $terms,

                 'operator' => $tax_operator

             );



            $count++;



        endwhile;



        if( $more_tax_queries ):

            $tax_relation = 'AND';

            if( isset( $original_atts['tax_relation'] ) && in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) ) )

                $tax_relation = $original_atts['tax_relation'];

            $args['tax_query']['relation'] = $tax_relation;

        endif;



        $args = array_merge( $args, $tax_args );

    }



    // If post parent attribute, set up parent

    if( $post_parent ) {

        if( 'current' == $post_parent ) {

            global $post;

            $post_parent = $post->ID;

        }

        $args['post_parent'] = intval( $post_parent );

    }



    // Set up html elements used to wrap the posts.

    // Default is ul/li, but can also be ol/li and div/div

    $wrapper_options = array( 'ul', 'ol', 'div' );

    if( ! in_array( $wrapper, $wrapper_options ) )

        $wrapper = 'ul';

    $inner_wrapper = 'div' == $wrapper ? 'div' : 'li';





    $listing = new WP_Query( apply_filters( 'display_posts_shortcode_args', $args, $original_atts ) );

    if ( ! $listing->have_posts() )

        return apply_filters( 'display_posts_shortcode_no_results', wpautop( $no_posts_message ) );



    $inner = '';

    while ( $listing->have_posts() ): $listing->the_post(); global $post;



        $image = $date = $excerpt = $content = '';



        $title = '<div class="row-fluid"><div class="span12"><h2><a class="title" href="' . apply_filters( 'the_permalink', get_permalink() ) . '">' . apply_filters( 'the_title', get_the_title() ) . '</a></h2></div></div><div class="clear"></div>';



        if ( $image_size && has_post_thumbnail() )

            $image = '<div class="span3 offset"><a class="image" href="' . get_permalink() . '">' . get_the_post_thumbnail( $post->ID, $image_size ) . '</a></div> ';



        if ( $include_date )

    //		$date = ' <span class="date">' . get_the_date( $date_format ) . '</span>';



        if ( $include_excerpt )

            $excerpt = '<div class="span12 offset excerpt">' . get_the_excerpt() . '</div>';



        if( $include_content )

            $content = '<div class="content">' . apply_filters( 'the_content', get_the_content() ) . '</div>';



        $class = array( 'listing-item' );

        $class = apply_filters( 'display_posts_shortcode_post_class', $class, $post, $listing );

        $output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">' . $title . $image . $date . $excerpt . $content . '</' . $inner_wrapper . '>';



        // If post is set to private, only show to logged in users

        if( 'private' == get_post_status( $post->ID ) && !current_user_can( 'read_private_posts' ) )

            $output = '';



        $inner .= apply_filters( 'display_posts_shortcode_output', $output, $original_atts, $title, $image, $date, $excerpt, $inner_wrapper, $content, $class );



    endwhile; wp_reset_postdata();



    $open = apply_filters( 'display_posts_shortcode_wrapper_open', '<' . $wrapper . ' class="row-fluid display-posts-listing">', $original_atts );

    $close = apply_filters( 'display_posts_shortcode_wrapper_close', '</' . $wrapper . '>', $original_atts );

    $return = $open . $inner . $close;



    return $return;

}


add_filter( 'excerpt_length', 'art_excerpt_length' );
function art_excerpt_length( $length ) {
    return 30; // pull first 10 words
}



//* Customize the post info function
add_filter( 'genesis_post_info', 'post_info_filter' );
function post_info_filter($post_info) {
if ( !is_page() ) {
    $post_info = '[post_date] [post_comments] [post_edit]';
    return $post_info;
}}


function remove_self_pinging(&$links,&$pung) {
    $self = get_option('siteurl');
    foreach ($links as $link => $data) {
        if (false !== strpos($link,$self))
            unset($links[$link]);
    }
}
add_action('pre_ping','remove_self_pinging',0,2);

/*
function art_redirect(){
  global $pagenow;
  if( 'wp-login.php' == $pagenow ) {
    if ( isset( $_POST['wp-submit'] ) ||
      ( isset($_GET['action']) && $_GET['action']=='logout') ||
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='confirm') ||
      ( isset($_GET['checkemail']) && $_GET['checkemail']=='registered') ) return;
    else wp_redirect(home_url('/wp-mk-login.php'));
    exit();
  }
}
add_action('init','art_redirect');
*/