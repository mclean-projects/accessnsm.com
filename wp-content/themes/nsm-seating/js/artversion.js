var $j = jQuery.noConflict();

/**
   $j(document).ready(function() {
        $j("#ticker").tweet({
          username: "MobilityNSM",
          page: 1,
          avatar_size: 32,
          count: 20,
          template: "{text}  {retweet_action}",
          loading_text: "loading ..."
        }).bind("loaded", function() {
          var ul = $j(this).find(".tweet_list");
          var ticker = function() {
            setTimeout(function() {
              var top = ul.position().top;
              var h = ul.height();
              var incr = (h / ul.children().length);
              var newTop = top - incr;
              if (h + newTop <= 0) newTop = 0;
              ul.animate( {top: newTop}, 900 );
              ticker();
            }, 15000);
          };
          ticker();
        $j(this).find("a.tweet_action").click(function(ev) {
          window.open(this.href, "Retweet",
                'menubar=0,resizable=0,width=550,height=420,top=200,left=400');
          ev.preventDefault();

        });
        });

});

*/




function setImageClass() {    switch(true) {
case($j(window).width()>1024):
$j("body").removeClass("w320 w480 w600 w800").addClass("w1024");
        break;
        case($j(window).width()>800): $j("body").removeClass("w320 w480 w600 w1024").addClass("w800");
        break;
        case($j(window).width()>600): $j("body").removeClass("w320 w480 w800 w1024").addClass("w600");
        break;
        case($j(window).width()>480): $j("body").removeClass("w320 w600 w800 w1024").addClass("w480");
        break;
        default: $j("body").removeClass("w480 w600 w800 w1024").addClass("w320");
        break;
    }
}

$j(document).ready(function() {
    setImageClass();
});

$j(window).resize(function() {
    setImageClass();
});


$j(document).ready(function() {
    /**
     * Enable newsticker for the twitter news in the homepage
     */
    $j('#ticker ul').newsticker(15000);

    $j('#slidertwo').bxSlider({
      mode: 'fade',
      speed: 800,
      pause: 7000,
      auto: true,
      autoControls: false,
      pager: true,
      controls: false
    });


    $j('.sliderthree').bxSlider({
      mode: 'fade',
      speed: 800,
      pause: 12000,
      auto: true,
      autoControls: false,
      pager: false,
      controls: false,
       captions: true
    });

    $j('#sliderfour').bxSlider({
      mode: 'fade',
      speed: 800,
      pause: 12000,
      auto: true,
      autoControls: false,
      pager: false,
      controls: false,
       captions: true
    });

    $j('#sliderfive').bxSlider({
          mode: 'fade',
          speed: 800,
          pause: 12000,
          auto: true,
          autoControls: false,
          pager: false,
          controls: false,
           captions: false
    });


    $j(".pop").popover();

    $j("a.pop").on("click", function(event){
          event.preventDefault();
    });


    $j("a.tp").tooltip({
       'selector': '',
       'placement': 'top'
    });


    $j('.ie7 .news-content p').filter(function () { return $j.trim(this.innerHTML) == "" }).remove();

    $j("input#cv").removeAttr('size');


    $j('li.no-lnk').attr('aria-haspopup', true);

    $j('li.no-pop').attr('aria-haspopup', false);

        $j("#footer-copyright h4.widgettitle").remove();

        $j('.rollover').each(function() {
            var std = $j(this).attr("src");
            var hover = std.replace("_nor.svg", "_hover.svg");
            $j(this).wrap('<div />').clone().insertAfter(this).attr('src', hover).removeClass('fadein').siblings().css({
                position:'absolute'
            });
            $j(this).mouseenter(function() {
                $j(this).stop().fadeTo(600, 0);
            }).mouseleave(function() {
                $j(this).stop().fadeTo(600, 1);
            });
        });


        $j(".sf-sub-indicator, .search-box .search-submit").remove();

        $j("#menu-footer-menu li a").after("<span class=\"bull\"> &bull; </span>");

              $j(window).scroll(function(){
                //if(jQuery(document).height() - jQuery(window).height() - jQuery(window).scrollTop() < 500){
                if($j(window).scrollTop() > 300){
                  $j('.gototop').stop().animate({opacity: 1});
                }
                else{
                  $j('.gototop').stop().animate({opacity: 0});
                }
              });



             $j('img, iframe').each(function(){
                $j(this).removeAttr('width')
                $j(this).removeAttr('height');
             });

            $j("img.a").hover(
            function() {
            $j(this).stop().animate({"opacity": "0"}, "slow");
            },
            function() {
            $j(this).stop().animate({"opacity": "1"}, "slow");
            });



    $j('#social-box img').hover(function() {
            $j(this).animate({ marginTop: '-5px' }, 200);
        }, function() {
            $j(this).animate({ marginTop: '5px' }, 200);
    });


        $j(".gototop").hide();

        $j(window).scroll(function () {
            if ($j(this).scrollTop() > 200) {
                $j('.gototop').fadeIn();
            } else {
                $j('.gototop').fadeOut();
            }
        });

        // scroll body to 0px on click
        $j('.gototop a').click(function () {
            $j('body,html').animate({
                scrollTop: 0
            }, 900);
            return false;
        });



});



