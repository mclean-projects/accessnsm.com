<?php
/**
 * Header file of the nsm-seating theme
 */

do_action( 'genesis_doctype' );
do_action( 'genesis_title' );
do_action( 'genesis_meta' );

wp_head(); //* we need this for plugins
?>
    <style>
        #mask {
          position: absolute;
          left: 0;
          top: 0;
          z-index: 9000;
          background-color: #000;
          display: none;
        }

        #boxes .window {
          position: absolute;
          left: 0;
          top: 0;
          width: 440px;
          height: 200px;
          display: none;
          z-index: 9999;
          padding: 20px;
          border-radius: 15px;
          text-align: center;
        }

        #boxes #dialog {
          width: 400px;
          height: 130px;
          padding: 10px;
          background-color: #ffffff;
          font-family: 'Segoe UI Light', sans-serif;
          font-size: 15pt;
        }

        #popupfoot {
          font-size: 16pt;
          position: absolute;
          bottom: 10px;
          width: 250px;
          left: 80px;
        }
        #dialog {
            left: 470px !important;
        }
        .close.agree {
            background: green none repeat scroll 0 0;
            border-radius: 4px;
            color: white;
            float: left;
            opacity: 1 !important;
            padding: 5px 14px;
        }
        .agree{

             background: red none repeat scroll 0 0;
            border-radius: 4px;
            color: white!important;
            opacity: 1 !important;
            padding: 5px 14px;
        }
    </style>
    <meta name="p:domain_verify" content="e4564b0416741611833220f41aa4e2ed"/>

    <link media="screen, projection" rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css"  />
    <link media="screen, projection" rel="stylesheet" type="text/css" href="/wp-content/themes/nsm-seating/stylesheets/screen.css" />
</head>
<?php
genesis_markup( array(
    'html5'   => '<body %s>',
    'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
    'context' => 'body',
) );
do_action( 'genesis_before' );

genesis_markup( array(
    'html5'   => '<div %s>',
    'xhtml'   => '<div id="wrap">',
    'context' => 'site-container',
) );

do_action( 'genesis_before_header' );
do_action( 'genesis_header' );
do_action( 'genesis_after_header' );

genesis_markup( array(
    'html5'   => '<div %s>',
    'xhtml'   => '<div id="inner">',
    'context' => 'site-inner',
) );
genesis_structural_wrap( 'site-inner' );
