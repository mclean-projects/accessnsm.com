<?php
/**
 * Homepage layout
 */
?>
<style>
.homebox.home-right {
  background: url(/wp-content/themes/nsm-seating/images/home-rightbgx2.jpg) no-repeat bottom right #fff;
  background-size: 165px 286px;
}
</style>

<?php get_header(); ?>
<?php genesis_before_content_sidebar_wrap(); ?>
<div id="content-sidebar-wrap">

<?php genesis_before_content(); ?>
<div id="content" class="hfeed">

<?php genesis_before_loop(); ?>

<?php if (is_home() || is_front_page()) {?>
<?php
    if ( is_active_sidebar( 'home-top' )) {
        echo '<div class="twit-box row-fluid">
                <a id="twitt-left" class="span3" href="https://twitter.com/intent/follow?screen_name=mobilitynsm">
                    <i class="icon-twitter"></i>
                    <h2>Follow us<br /> on Twitter</h2>
                </a>
                <div id="ticker" class="span9">';
                dynamic_sidebar( 'home-top' );
        echo   '</div>
            </div>
        ';
    }

    if ( is_active_sidebar( 'home-left' ) || is_active_sidebar( 'home-right' ) ) {
        echo '<div id="home">';
            echo '<div class="clear"></div>';
            echo '<div class="row-fluid">';

            echo '<div class="home-left span6 homebox">';
            dynamic_sidebar( 'home-left' );
            echo '</div>';

            echo '<div class="span6 homebox home-right">';
            dynamic_sidebar( 'home-right' );
            echo '</div>';
            echo '</div>';
        echo '</div>';
    }
?>
<?php }?>	<?php genesis_after_loop(); ?>
</div><!-- end #content -->
<?php genesis_after_content(); ?>
</div><!-- end #content-sidebar-wrap -->
<?php genesis_after_content_sidebar_wrap(); ?>
<?php get_footer(); ?>